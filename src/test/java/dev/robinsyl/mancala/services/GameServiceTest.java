package dev.robinsyl.mancala.services;

import dev.robinsyl.mancala.config.BoardConfig;
import dev.robinsyl.mancala.types.Board;
import dev.robinsyl.mancala.types.GameState;
import dev.robinsyl.mancala.types.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

class GameServiceTest {

    GameService service;

    @BeforeEach
    void setUp() {
        BoardConfig config = new BoardConfig(14, 6);
        service = new GameService(config);
    }

    @Test
    void moveNormalEnd() {
        service.move(2);
        GameState state = service.getGameState();
        assertThat(state.boardState(), contains(6, 6, 0, 7, 7, 7, 1, 7, 7, 6, 6, 6, 6, 0));
        assertThat(state.currentPlayer(), equalTo(Player.TWO));
        assertThat(state.active(), is(true));
        assertThat(state.winner(), nullValue());
    }

    @Test
    void moveEndsInPlayerBigPit() {
        service.move(0);
        GameState state = service.getGameState();
        assertThat(state.boardState(), contains(0, 7, 7, 7, 7, 7, 1, 6, 6, 6, 6, 6, 6, 0));
        // Player gets another turn
        assertThat(state.currentPlayer(), equalTo(Player.ONE));
        assertThat(state.active(), is(true));
        assertThat(state.winner(), nullValue());
    }

    @Test
    void moveCausesSmallPitCapture() {
        // technically an impossible game state just to force a capture to happen
        Board board = new Board(List.of(3, 6, 6, 0, 6, 6, 0, 6, 6, 6, 6, 6, 6, 0));
        service.setGameState(board, Player.ONE);
        service.move(0);
        GameState state = service.getGameState();
        // The opponent's adjacent small pit is captured
        assertThat(state.boardState(), contains(0, 7, 7, 7, 6, 6, 0, 6, 6, 0, 6, 6, 6, 0));
        assertThat(state.currentPlayer(), equalTo(Player.TWO));
        assertThat(state.active(), is(true));
        assertThat(state.winner(), nullValue());
    }

    @Test
    void moveCausesLargePitCapture() {
        // technically an impossible game state just to force a capture to happen
        Board board = new Board(List.of(6, 6, 6, 6, 6, 6, 4, 6, 6, 4, 6, 6, 6, 0));
        service.setGameState(board, Player.TWO);
        service.move(9);
        GameState state = service.getGameState();
        // The opponent's big pit is captured
        assertThat(state.boardState(), contains(6, 6, 6, 6, 6, 6, 0, 6, 6, 0, 7, 7, 7, 5));
        // Player gets another turn as the turn ended in the player's big pit
        assertThat(state.currentPlayer(), equalTo(Player.TWO));
        assertThat(state.active(), is(true));
        assertThat(state.winner(), nullValue());
    }

    @Test
    void illegalMoveNotPlayerPit() {
        // technically an impossible game state
        Board board = new Board(List.of(6, 6, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 0));
        service.setGameState(board, Player.ONE);
        assertThrows(IllegalArgumentException.class, () -> service.move(8));
        service.setGameState(board, Player.TWO);
        assertThrows(IllegalArgumentException.class, () -> service.move(2));
    }

    @Test
    void illegalMoveIsBigPit() {
        Board board = new Board(List.of(6, 6, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 0));
        service.setGameState(board, Player.ONE);
        assertThrows(IllegalArgumentException.class, () -> service.move(6));
        service.setGameState(board, Player.TWO);
        assertThrows(IllegalArgumentException.class, () -> service.move(13));
    }

    @Test
    void illegalMoveEmptySmallPit() {
        // technically an impossible game state
        Board board = new Board(List.of(6, 6, 6, 0, 6, 6, 0, 6, 6, 6, 0, 6, 6, 0));
        service.setGameState(board, Player.ONE);
        assertThrows(IllegalArgumentException.class, () -> service.move(3));
        service.setGameState(board, Player.TWO);
        assertThrows(IllegalArgumentException.class, () -> service.move(10));
    }

    @Test
    void playerOneWon() {
        // technically an impossible game state
        Board board = new Board(List.of(0, 0, 0, 0, 0, 0, 12, 6, 6, 6, 0, 6, 6, 2));
        service.setGameState(board, Player.ONE);
        GameState gameState = service.getGameState();
        assertThat(gameState.active(), is(false));
        assertThat(gameState.winner(), equalTo(Player.ONE));
    }

    @Test
    void playerTwoWon() {
        // technically an impossible game state
        Board board = new Board(List.of(0, 0, 0, 0, 0, 0, 2, 6, 6, 6, 0, 6, 6, 12));
        service.setGameState(board, Player.ONE);
        GameState gameState = service.getGameState();
        assertThat(gameState.active(), is(false));
        assertThat(gameState.winner(), equalTo(Player.TWO));
    }

    @Test
    void gameTied() {
        // technically an impossible game state
        Board board = new Board(List.of(0, 0, 0, 0, 0, 0, 12, 6, 6, 6, 0, 6, 6, 12));
        service.setGameState(board, Player.ONE);
        GameState state = service.getGameState();
        assertThat(state.active(), is(false));
        assertThat(state.winner(), nullValue());
    }

    @Test
    void cannotMoveAfterEnd() {
        // technically an impossible game state
        Board board = new Board(List.of(0, 0, 0, 0, 0, 0, 12, 6, 6, 6, 0, 6, 6, 2));
        service.setGameState(board, Player.ONE);
        assertThrows(IllegalArgumentException.class, () -> service.move(3));
    }
}