package dev.robinsyl.mancala.types;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;

class BoardTest {

    @Test
    void initializeBoard() {
        Board board = new Board(14, 6);
        assertThat(board.getGameState(), hasSize(14));
        assertThat(board.getStoneCount(5), equalTo(6));
    }

    @Test
    void invalidInitialization() {
        assertThrows(IllegalArgumentException.class, () -> new Board(13, 6));
    }

    @Test
    void increment() {
        Board board = new Board(14, 6);
        board.increment(5);
        assertThat(board.getGameState().get(5), equalTo(7));
    }

    @Test
    void decrement() {
        Board board = new Board(14, 6);
        board.decrement(3);
        assertThat(board.getGameState().get(3), equalTo(5));
    }

    @Test
    void empty() {
        Board board = new Board(14, 6);
        int stones = board.empty(3);
        assertThat(stones, equalTo(6));
        assertThat(board.getStoneCount(3), equalTo(0));
    }

    @Test
    void isOpponentBigPit() {
        // pattern: player small pit, player big pit, opponent big pit
        Board board = new Board(14, 6);
        assertFalse(board.isOpponentBigPit(Player.ONE, 2));
        assertFalse(board.isOpponentBigPit(Player.ONE, 6));
        assertTrue(board.isOpponentBigPit(Player.ONE, 13));
        assertFalse(board.isOpponentBigPit(Player.TWO, 2));
        assertFalse(board.isOpponentBigPit(Player.TWO, 13));
        assertTrue(board.isOpponentBigPit(Player.TWO, 6));

        Board board2 = new Board(20, 2);
        assertFalse(board2.isOpponentBigPit(Player.ONE, 2));
        assertFalse(board2.isOpponentBigPit(Player.ONE, 9));
        assertTrue(board2.isOpponentBigPit(Player.ONE, 19));
        assertFalse(board2.isOpponentBigPit(Player.TWO, 2));
        assertFalse(board2.isOpponentBigPit(Player.TWO, 19));
        assertTrue(board2.isOpponentBigPit(Player.TWO, 9));
    }

    @Test
    void isPlayerSmallPit() {
        // pattern: opponent small pit, player big pit, player small pit
        Board board = new Board(14, 6);
        assertFalse(board.isPlayerSmallPit(Player.ONE, 12));
        assertFalse(board.isPlayerSmallPit(Player.ONE, 6));
        assertTrue(board.isPlayerSmallPit(Player.ONE, 3));
        assertFalse(board.isPlayerSmallPit(Player.TWO, 2));
        assertFalse(board.isPlayerSmallPit(Player.TWO, 13));
        assertTrue(board.isPlayerSmallPit(Player.TWO, 12));

        Board board2 = new Board(20, 2);
        assertFalse(board2.isPlayerSmallPit(Player.ONE, 12));
        assertFalse(board2.isPlayerSmallPit(Player.ONE, 9));
        assertTrue(board2.isPlayerSmallPit(Player.ONE, 3));
        assertFalse(board2.isPlayerSmallPit(Player.TWO, 2));
        assertFalse(board2.isPlayerSmallPit(Player.TWO, 19));
        assertTrue(board2.isPlayerSmallPit(Player.TWO, 12));
    }

    @Test
    void isPlayerBigPit() {
        // pattern: player small pit, opponent big pit, player big pit
        Board board = new Board(14, 6);
        assertFalse(board.isPlayerBigPit(Player.ONE, 2));
        assertFalse(board.isPlayerBigPit(Player.ONE, 13));
        assertTrue(board.isPlayerBigPit(Player.ONE, 6));
        assertFalse(board.isPlayerBigPit(Player.TWO, 2));
        assertFalse(board.isPlayerBigPit(Player.TWO, 6));
        assertTrue(board.isPlayerBigPit(Player.TWO, 13));

        Board board2 = new Board(20, 2);
        assertFalse(board2.isPlayerBigPit(Player.ONE, 2));
        assertFalse(board2.isPlayerBigPit(Player.ONE, 19));
        assertTrue(board2.isPlayerBigPit(Player.ONE, 9));
        assertFalse(board2.isPlayerBigPit(Player.TWO, 2));
        assertFalse(board2.isPlayerBigPit(Player.TWO, 9));
        assertTrue(board2.isPlayerBigPit(Player.TWO, 19));
    }

    @Test
    void getSize() {
        Board board = new Board(20, 6);
        assertThat(board.getSize(), equalTo(20));
    }
}