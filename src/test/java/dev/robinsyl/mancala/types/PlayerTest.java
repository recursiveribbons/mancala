package dev.robinsyl.mancala.types;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

class PlayerTest {
    @Test
    void nextPlayer() {
        assertThat(Player.ONE.nextPlayer(), equalTo(Player.TWO));
        assertThat(Player.TWO.nextPlayer(), equalTo(Player.ONE));
    }
}