package dev.robinsyl.mancala.types;

import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Nullable;

import java.util.List;

public record GameState(
        @ArraySchema(arraySchema = @Schema(description = "the board state, indexed by pit and number of stones in each pit", multipleOf = 2), schema = @Schema(description = "number of stones")) List<Integer> boardState,
        @Schema(description = "the number of the current player") Player currentPlayer,
        @Schema(description = "whether the game is ongoing or has ended") boolean active,
        @Schema(description = "if the game has ended, who won", nullable = true) @Nullable Player winner) {
}
