package dev.robinsyl.mancala.types;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Board {
    private final List<Integer> pits;
    private final int BIG_PIT_1;
    private final int BIG_PIT_2;

    public Board(int boardSize, int stonesPerHole) {
        if (boardSize % 2 != 0) {
            throw new IllegalArgumentException("Board size must be even");
        }
        BIG_PIT_1 = boardSize / 2 - 1;
        BIG_PIT_2 = boardSize - 1;
        pits = new ArrayList<>(Collections.nCopies(boardSize, stonesPerHole));
        pits.set(BIG_PIT_1, 0);
        pits.set(BIG_PIT_2, 0);
    }

    /**
     * This is a constructor meant for testing which forces a certain state of the board
     *
     * @param pits the state of the board
     */
    public Board(List<Integer> pits) {
        if (pits.size() % 2 != 0) {
            throw new IllegalArgumentException("Board size must be even");
        }
        BIG_PIT_1 = pits.size() / 2 - 1;
        BIG_PIT_2 = pits.size() - 1;
        this.pits = new ArrayList<>(pits);
    }

    public void increment(int pit) {
        assertValidPit(pit);
        pits.set(pit, pits.get(pit) + 1);
    }

    public void increment(int pit, int stones) {
        assertValidPit(pit);
        pits.set(pit, pits.get(pit) + stones);
    }

    public void decrement(int pit) {
        assertValidPit(pit);
        pits.set(pit, pits.get(pit) - 1);
    }

    public int getStoneCount(int pit) {
        assertValidPit(pit);
        return pits.get(pit);
    }

    public int empty(int pit) {
        assertValidPit(pit);
        return pits.set(pit, 0);
    }

    public boolean isOpponentBigPit(Player player, int pit) {
        assertValidPit(pit);
        return switch (player) {
            case ONE -> pit == BIG_PIT_2;
            case TWO -> pit == BIG_PIT_1;
        };
    }

    public boolean isPlayerSmallPit(Player player, int pit) {
        assertValidPit(pit);
        return switch (player) {
            case ONE -> pit < BIG_PIT_1;
            case TWO -> pit > BIG_PIT_1 && pit < BIG_PIT_2;
        };
    }

    public boolean isPlayerBigPit(Player player, int pit) {
        assertValidPit(pit);
        return switch (player) {
            case ONE -> pit == BIG_PIT_1;
            case TWO -> pit == BIG_PIT_2;
        };
    }

    public int getPlayerBigPit(Player player) {
        return switch (player) {
            case ONE -> BIG_PIT_1;
            case TWO -> BIG_PIT_2;
        };
    }

    public int getOpponentBigPit(Player player) {
        return switch (player) {
            case ONE -> BIG_PIT_2;
            case TWO -> BIG_PIT_1;
        };
    }

    public List<Integer> getGameState() {
        return Collections.unmodifiableList(pits);
    }

    public int getSize() {
        return pits.size();
    }

    private void assertValidPit(int pit) {
        if (pit >= pits.size()) {
            throw new IllegalArgumentException("Not a valid pit index");
        }
    }
}
