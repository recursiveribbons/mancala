package dev.robinsyl.mancala.types;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Player {
    ONE, TWO;

    public Player nextPlayer() {
        if (this == ONE) {
            return TWO;
        }
        return ONE;
    }

    @JsonValue
    public int value() {
        return switch (this) {
            case ONE -> 1;
            case TWO -> 2;
        };
    }
}
