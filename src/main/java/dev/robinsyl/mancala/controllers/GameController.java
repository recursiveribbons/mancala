package dev.robinsyl.mancala.controllers;

import dev.robinsyl.mancala.services.GameService;
import dev.robinsyl.mancala.types.GameState;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("mancala")
public class GameController {
    private final GameService service;

    public GameController(GameService service) {
        this.service = service;
    }

    @Operation(summary = "Fetch the current game state")
    @ApiResponse(responseCode = "200", description = "OK")
    @GetMapping("state")
    public GameState move() {
        return service.getGameState();
    }

    @PostMapping("move/{pit}")
    @Operation(summary = "Make a move and return the current game state")
    @ApiResponse(responseCode = "200", description = "OK", content = @Content(schema = @Schema(implementation = GameState.class, description = "The current game state")))
    @ApiResponse(responseCode = "400", description = "Invalid move", content = @Content(mediaType = MediaType.APPLICATION_PROBLEM_JSON_VALUE))
    public GameState move(@PathVariable @Parameter(description = "The pit ID chosen by the player") int pit) {
        try {
            service.move(pit);
        } catch (IllegalArgumentException e) {
            // may need type information and proper error message localization in the future
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
        return service.getGameState();
    }
}
