package dev.robinsyl.mancala.config;

import jakarta.validation.constraints.Positive;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.bind.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

@ConfigurationProperties("game.board")
@Validated
public class BoardConfig {
    /**
     * Total size of game board, including large and small pits, must be even
     */
    @Positive
    private final int size;

    /**
     * Number of stones per small pit at the start of the game
     */
    @Positive
    private final int stonesPerPit;

    @ConstructorBinding
    public BoardConfig(int size, int stonesPerPit) {
        this.size = size;
        this.stonesPerPit = stonesPerPit;
    }

    public int getSize() {
        return size;
    }

    public int getStonesPerPit() {
        return stonesPerPit;
    }
}
