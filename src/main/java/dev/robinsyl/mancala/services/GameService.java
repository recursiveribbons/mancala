package dev.robinsyl.mancala.services;

import dev.robinsyl.mancala.config.BoardConfig;
import dev.robinsyl.mancala.types.Board;
import dev.robinsyl.mancala.types.GameState;
import dev.robinsyl.mancala.types.Player;
import org.springframework.stereotype.Service;

@Service
public class GameService {
    private final BoardConfig config;
    private Board board;
    private Player currentPlayer;

    public GameService(BoardConfig config) {
        this.config = config;
        newGame();
    }

    public void newGame() {
        board = new Board(config.getSize(), config.getStonesPerPit());
        currentPlayer = Player.ONE;
    }

    public void move(int pit) {
        if (!isGameActive()) {
            throw new IllegalArgumentException("The game has ended!");
        }
        if (!board.isPlayerSmallPit(currentPlayer, pit)) {
            throw new IllegalArgumentException("Not a valid move! This is not your small pit");
        }
        int stones = board.empty(pit);
        if (stones == 0) {
            throw new IllegalArgumentException("Not a valid move! You chose an empty pit");
        }
        do {
            // Move to next pit
            pit++;
            if (pit >= board.getSize()) {
                pit = 0;
            }
            // Do not drop stones in the opponent's big pit
            if (board.isOpponentBigPit(currentPlayer, pit)) {
                continue;
            }
            // Put stone in pit
            board.increment(pit);
            stones--;
        } while (stones > 0);
        // Check if final pit was empty, as in it only has the stone we dropped in.
        // Also check if it's a player's pit, either big or small.
        // If so, perform capture to opposing pit
        if (board.isPlayerSmallPit(currentPlayer, pit)) {
            int largestPitIndex = board.getSize() - 2;
            int captured = board.empty(largestPitIndex - pit);
            board.increment(pit, captured);
        } else if (board.isPlayerBigPit(currentPlayer, pit)) {
            int captured = board.empty(board.getOpponentBigPit(currentPlayer));
            board.increment(board.getPlayerBigPit(currentPlayer), captured);
        }
        // Check if the move ended in the player's big pit
        // If not, move to next player. Otherwise, same player goes again
        if (!board.isPlayerBigPit(currentPlayer, pit)) {
            currentPlayer = currentPlayer.nextPlayer();
        }
    }

    public GameState getGameState() {
        boolean active = isGameActive();
        Player winner = null;
        if (!active) {
            winner = determineWinner();
        }
        return new GameState(board.getGameState(), currentPlayer, active, winner);
    }

    private boolean isGameActive() {
        int stones = 0;
        for (int i = 0; i < board.getPlayerBigPit(Player.ONE); i++) {
            stones += board.getStoneCount(i);
        }
        if (stones == 0) {
            return false;
        }
        stones = 0;
        for (int i = board.getPlayerBigPit(Player.ONE) + 1; i < board.getPlayerBigPit(Player.TWO); i++) {
            stones += board.getStoneCount(i);
        }
        return stones != 0;
    }

    private Player determineWinner() {
        int score1 = board.getStoneCount(board.getPlayerBigPit(Player.ONE));
        int score2 = board.getStoneCount(board.getPlayerBigPit(Player.TWO));
        if (score1 > score2) {
            return Player.ONE;
        } else if (score2 > score1) {
            return Player.TWO;
        }
        return null;
    }

    /**
     * This method is only to be used in tests to force a certain game state
     *
     * @param board         board state
     * @param currentPlayer current player
     */
    void setGameState(Board board, Player currentPlayer) {
        this.board = board;
        this.currentPlayer = currentPlayer;
    }
}
