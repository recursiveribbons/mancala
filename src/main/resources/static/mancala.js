function load() {
    fetch("http://localhost:8080/mancala/state")
        .then(res => res.json())
        .then(data => drawBoard(data));
}

function drawBoard(data) {
    if (data.detail) {
        document.getElementById("alert").innerText = data.detail;
        return;
    }
    document.getElementById("alert").innerText = '';
    if (!data.active) {
        if (data.winner) {
            document.getElementById("alert").innerText = `Player ${data.winner} has won`;
        } else {
            document.getElementById("alert").innerText = `The game has tied`;
        }
    }
    document.getElementById("current-player").innerText = data.currentPlayer;
    const size = data.boardState.length;
    let row = document.getElementById("player-1");
    clearChildren(row);
    createBigCell(row, data.boardState[size / 2 - 1]);
    for (let i = size / 2 - 2; i >= 0; i--) {
        createCell(row, i, data.boardState[i], data.active);
    }
    createEmptyCell(row);
    row = document.getElementById("player-2");
    clearChildren(row);
    createEmptyCell(row);
    for (let i = size / 2; i < size - 1; i++) {
        createCell(row, i, data.boardState[i], data.active);
    }
    createBigCell(row, data.boardState[size - 1]);
}

function clearChildren(element) {
    while (element.firstChild) {
        element.removeChild(element.lastChild);
    }
}

function createCell(row, id, stones, active) {
    const cell = document.createElement("td");
    cell.id = id;
    if (active) {
        cell.addEventListener("click", move);
    }
    const cellText = document.createTextNode(stones);
    cell.appendChild(cellText);
    row.appendChild(cell);
}

function createEmptyCell(row) {
    const cell = document.createElement("td");
    row.appendChild(cell);
}

function createBigCell(row, stones) {
    const cell = document.createElement("td");
    const cellText = document.createElement("strong");
    cellText.innerText = stones;
    cell.appendChild(cellText);
    row.appendChild(cell);
}

function move(event) {
    const id = event.target.id;
    fetch(`http://localhost:8080/mancala/move/${id}`, {method: "POST"})
        .then(res => res.json())
        .then(data => drawBoard(data));
}
