FROM gradle:8.0.2-jdk17 as buildstep
WORKDIR /app
COPY . .
RUN gradle bootJar

FROM eclipse-temurin:17-jre
COPY --from=buildstep /app/build/libs/*.jar /opt/mancala/lib/app.jar
ENTRYPOINT ["java"]
CMD ["-jar", "/opt/mancala/lib/app.jar"]
