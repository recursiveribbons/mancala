# Mancala

This is an implementation of the game Mancala as based on requirements from Bol

## How to run

Use one of the methods below to run the application. The GUI will be available at http://localhost:8080/mancala.html

### IDE

Import the project into your IDE of choice and run
the [MancalaApplication](src/main/java/dev/robinsyl/mancala/MancalaApplication.java) class

### Gradle

Run `./gradlew bootRun` in the terminal

### Docker

Run the following in the terminal

```bash
docker build -t mancala .
docker run --rm -it -p 8080:8080 mancala
```

### Docker Compose

Run `docker-compose up --build` in the terminal

## API documentation

Visit `/swagger-ui.html` to see the Swagger UI, and `/v3/api-docs` for the JSON formatted OpenAPI spec.

## Architecture

The application is based on Spring Boot 3 and Spring Boot Web, running on Java 17.
GitLab CI is configured for tests, coverage, and building a Docker image.

## Future considerations

1. Persisted game states in a database, identified by tokens or accounts
2. Multiple concurrent games
3. Games that can be played on multiple clients instead of local multiplayer
